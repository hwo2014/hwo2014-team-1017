#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;

    if (msg_type == "turbo") {
        std::cout << r.to_string() << std::endl;
    }
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    //return jsoncons::json::parse_string("{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\": \"runforrestrun\", \"key\": \"QueoXotsEoYr5w\" }, \"trackName\": \"usa\", \"carCount\": 1 }}");
    //return jsoncons::json::parse_string("{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\": \"runforrestrun\", \"key\": \"QueoXotsEoYr5w\" }, \"trackName\": \"germany\", \"carCount\": 1 }}");
    //return jsoncons::json::parse_string("{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\": \"runforrestrun\", \"key\": \"QueoXotsEoYr5w\" }, \"trackName\": \"france\", \"carCount\": 1 }}");

    jsoncons::json data;

    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);

    jsoncons::json dataBot;
    dataBot["name"]= name;
    dataBot["key"]= key;
    data["botId"] = dataBot;
    data["carCount"] = 3;
    //data["carCount"] = 2;
    data["trackName"] = "keimola";
    data["password"] ="forrest";
    data["laps"] = 6;
    data["lap"] = 6;
    return make_request("createRace", data);
    //return make_request("joinRace", data);
  }

  jsoncons::json make_ping() {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_turbo() {
      jsoncons::json data;
      data["dummy"] = "activate turbo";
      return make_request("turbo", "dummy data");
  }

  jsoncons::json make_throttle(double throttle) {
    return make_request("throttle", throttle);
  }

  jsoncons::json make_switch(bool isLeft) {
      return make_request("switchLane", isLeft ? "Left" : "Right");
  }

  // {"msgType": "switchLane", "data": "Left"}

}  // namespace hwo_protocol
