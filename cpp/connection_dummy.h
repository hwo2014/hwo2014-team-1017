#ifndef DUMMY_CONNECTION_H
#define DUMMY_CONNECTION_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track.h"

class dummy_connection
{
public:
   dummy_connection() : state{0}, countMessages{0}, throttle{0}, pieceId{0}, distance{0}, gameTick{0}, speed{0}, lap{0}, ticksLapStart{0}, bestLap{100000} {}
  //~dummy_connection();
  jsoncons::json receive_response();
  void send_requests(const std::vector<jsoncons::json>& msgs);
  void setTrack(const Track2& track);

private:
  Track2 mTrack;
  int state;
  int countMessages;
  double throttle;
  int pieceId;
  double distance;
  int gameTick;
  double speed;
  int lap;
  int ticksLapStart;
  int bestLap;

};

#endif
