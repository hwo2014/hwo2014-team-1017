#pragma once
//#ifndef BOT_H
//#define BOT_H

#include <iostream>
#include <jsoncons/json.hpp>
#include "track.h"
#include "util.h"


class SpeedEstimator {
};

class Bot {
public:
    Bot() :  mCarColor{}, mCarId{-1}, mGameTick{0}, mThrottle{0}, mLog{}, mIsCrashed{false},
        mIsTurboAvailable{false}, lastSwitchHash{-1}, mIsQuickRace{false}, mNumberOfLaps{3},
        mIsTuboPrepared{false} {};
    //void onCarPositions(const jsoncons::json& msg, int gameTick);
    void selectAction(int gameTick, Track2& track2);
    void onGameInit(const Track2& track2, bool isQuickRace, int numberOfLaps);
    void onYourCar(const jsoncons::json& msg);
    void onGameEnd(const Track2& track);
    void onTurboAvailable(const jsoncons::json& msg);
    double getThrottle();


    int isSwitch();
    bool isTurbo();

    void setCrashed(bool isCrashed) {
        mIsCrashed = isCrashed;
    }

   // const Track& getTrack();

private:
    string mCarColor;
    int mCarId;
    int mGameTick;
    double mThrottle;
    //double mPrevThrottle;
    Log mLog;
    bool mIsCrashed;
    bool mIsTurboAvailable;
    int lastSwitchHash;
    bool mIsQuickRace;
    int mNumberOfLaps;
    bool mIsTuboPrepared;

    int selectNextSection(const Track2& track, const CarPosition2& myPosition);

    //void calcMaxSpeeds(const Track& track);
};

//#endif // BOT_H
