#pragma once

#include <iostream>
#include <vector>
#include <jsoncons/json.hpp>

using namespace std;



enum class Turn {none, left, right };

const double pi = 3.1415926536;



//////////////////////////////////////////////////////////


///////////////////////////

struct Track2;
struct LaneInfo;
struct PieceInfo;
struct CarInfo;
struct Sector;
typedef vector<LaneInfo> LaneInfos;
typedef vector<PieceInfo> PieceInfos;
typedef vector<CarInfo> CarInfos;
typedef vector<Sector> Sectors;

struct LaneInfo {
    int id;
    double distanceFromCenter;
    int laneToLeft;
    int laneToRight;
    LaneInfo(int id, double distanceFromCenter, int laneToLeft, int laneToRight) :
        id{id}, distanceFromCenter{distanceFromCenter}, laneToLeft{laneToLeft}, laneToRight{laneToRight} {}
};

struct PieceInfo {
    int id;
    Turn turn;
    double length;
    double radius;
    double angle;
    //double maxSpeed;
    bool isSwitch;
    bool isTurbo;
    PieceInfo(int id, Turn turn, double length, double radius, double angle, bool isSwitch) :
        id{id}, turn{turn}, length{length}, radius{radius}, angle{angle}, isSwitch{isSwitch}, isTurbo{false} {}
};

struct CarPosition2 {
   int pieceId;
   int laneId;
   int laneIdStart;
   int laneIdEnd;
   int sectorId;
   double pieceDistance;
   double angle;
   int lap;
   double speed;
   double totalDistance;

   CarPosition2() {
      pieceId = -1;
      laneId = -1;
      laneIdStart = -1;
      laneIdEnd = -1;
      sectorId = -1;
      pieceDistance = -1;
      angle = -1;
      lap = -1;
      speed = -1;
      totalDistance = -1;
   }

   bool isInited() const { return pieceId >= 0; }
};

struct CarInfo {
    int id;
    string color;
    bool isCrashed;
    CarPosition2 position;
    CarPosition2 positionOld;
    CarInfo(int id, const string& color);

    const CarPosition2& getPosition() const;
    const CarPosition2& getPositionOld() const;

    void updatePosition(const Track2& track, const CarPosition2& newPos, int deltaGameTicks);
};


//struct SpeedPoint {
//    double distance;
//    double speedMax;
//    double speedEstimate;
//    SpeedPoint(double distance, double speedMax, double speedEstimate) :
//       distance{distance}, speedMax{speedMax}, speedEstimate{speedEstimate} {}
//};

struct SpeedPoint {
    double speed;
    double angle;
    double angleFactor;
    SpeedPoint(double speed, double angle, double angleFactor) :
       speed{speed}, angle{abs(angle)}, angleFactor{angleFactor} {}
};

struct SpeedBucket {
    int id;
    int pieceId;
    double radius;
    double maxSpeed;
    double maxSpeedFuture;
    double pieceDistance;
    double length;
    bool isTurn;
    //double angleFactor;
    //bool isFunctionAvailable;
    //double initial;
    //double slope;
    //double avgError;

    vector<SpeedPoint> speedPoints;

    SpeedBucket(int id, int pieceId, double radius, double maxSpeed, double pieceDistance, double length, bool isTurn) :
        id{id}, pieceId{pieceId}, radius{radius}, maxSpeed{maxSpeed}, maxSpeedFuture{maxSpeed}, pieceDistance{pieceDistance}, length{length}, isTurn{isTurn} {}

    void addPoint(const SpeedPoint& point) {
        speedPoints.push_back(point);
    }
};


class SpeedLimit {
public:
    //SpeedLimit(const vector<SpeedBucket>& buckets, const vector<double>& bucketLengts) : buckets{buckets}, bucketLengths{bucketLengths};
    SpeedLimit() : buckets{} {}

    void addBuckets(int pieceId, double radius, int bucketCount, double maxSpeed, double length, bool isTurn) {
        double bucketLength = length / bucketCount;
        double pieceDistance = 0;
        for (int i=0; i<bucketCount; ++i) {
            int id = buckets.size();
            buckets.push_back(SpeedBucket{id, pieceId, radius, maxSpeed, pieceDistance, bucketLength, isTurn});
            pieceDistance += bucketLength;
        }
    }

    void addSample(int pieceId, double pieceDistance, const SpeedPoint& point);

    //void fix(bool isLastLap, int carPieceId);
    void fix();
    double getLimitAtDistance(int pieceId, double pieceDistance, double currSpeed, bool isLastLap);
    //void adaptBack();
    void log() const;
    void logLimit() const;
    //void updateValues();
    void clear() {
        buckets.clear();
        //bucketLengths.clear();
    }
    static const int bucketsPerPiece = 5;
    vector<SpeedBucket> buckets;
    //vector<double> bucketLengths;



private:
    SpeedBucket& getBucket(int pieceId, double pieceDistance);
    //void fixMaxSpeed();
    //double findTime(const vector<double>& distances, double searchedDistance);
};



class Lane2;
typedef vector<Lane2> Lane2s;

class Lane2 {
public:
    int id;
    SpeedLimit speedLimit;
    vector<double> pieceLengths;

    Lane2(int id, const SpeedLimit& speedLimit, const vector<double>& pieceLengths) :
       id{id}, speedLimit{speedLimit}, pieceLengths{pieceLengths} {}
};


struct Sector {
   int id;
   int laneId;
   int pieceId;
   double startDistance;
   double endDistance;
   double deltaDistance;
   //double distanceWhenSwitch;
   double distanceToGoal;

   int closestIndex;
   vector<int> nextSectors;

   Sector(int id, int laneId, int pieceId, double startDistance, double endDistance, double deltaDistance)
      : id{id}, laneId{laneId}, pieceId{pieceId}, startDistance{startDistance}, endDistance{endDistance}, deltaDistance{deltaDistance},
        distanceToGoal{deltaDistance}, closestIndex{0}, nextSectors{} {}

   void addNextSector(int sectorId) {
      //cout << "link sector " << id << " to " << sectorId << endl;
      nextSectors.push_back(sectorId);
   }
};

class PhysicsLog;
typedef vector<PhysicsLog> PhysicsLogs;

struct PhysicsLog {
    int gameTick;
    int pieceId;
    double pieceDistance;
    //double prevSpeed;
    double throttle;
    double radius;
    //double prevAngle;
    double speed;
    double angle;

    //double radius;

    PhysicsLog() :
            gameTick{-1},
            throttle{-1},
            radius{-1},
            speed{-1},
            angle{-1} {
    }
    PhysicsLog(int gameTick, int pieceId, double throttle, double radius, double speed, double angle) :
            gameTick{gameTick},
            pieceId{pieceId},
            throttle{throttle},
            radius{radius},
            speed{speed},
            angle{angle} {
    }
};

struct AngleLog {
    int pieceId;
    double avgSpeed;
    double maxAngle;
    double radius;
};

class Physics {
public:

    static const Physics& p() { return instance; }

    double calcDeltaSpeed(double speed, double throttle) const {
        return  throttle * power - speed * drag;
    }

    double adaptThrottle(double currSpeed, double targetSpeed) const {
        double deltaSpeed = targetSpeed - currSpeed;
        double throttle = (deltaSpeed + currSpeed * drag) / power;
        return max(0.0, min(1.0, throttle));
    }

    double maxSpeed() const {
        return power / drag;
    }

    double maxDeltaAngle(double radius, double speed) const {
        return -125.2243 + 395.7098 * speed * speed / radius;
    }

    double calcMaxSpeed(double radius, double maxAngle) const {
        double speed = sqrt(abs(radius) * (abs(maxAngle) + 125.2243) / 395.7098);
        // todo: turbo?
        return min(speed, Physics::p().maxSpeed()*10);
    }

//    double calcEstimatedSpeed(double radius, double maxAngle) {
//        //double speed = sqrt(abs(radius) * (abs(maxAngle) + 125.2243) / 395.7098);
//        // todo: turbo?
//        return min(Physics::p().maxSpeed(), calcMaxSpeed(radius, maxAngle));
//    }

    static void log(const PhysicsLog& log, const Track2& track) {
        Physics::log(instance, log);
    }

    bool isSpeedInitialized() const {
        return isPowerInited && isDragInited;
    }

//        //cout << "Physics: speed:" << log.speed << " throttle:" << log.throttle << endl;
//        if (!isPowerInited && prevSpeed == 0 && prevThrottle == 1) {
//            power = log.speed;
//            isPowerInited = true;
//            cout << "power " << power;
//        }
//        if (isPowerInited && !isDragInited && log.speed > 0) {
//            double d = (prevThrottle * power - (log.speed - prevSpeed)) / log.speed;
//            cout << "estimate drag " << d;
//        }
//        prevSpeed = log.speed;
//        prevThrottle = log.throttle;
//    }

    static void LogAll() {
        cout << "Physics::logAll" << endl;
        Physics::logAll(instance);
    }

    static void setTurbo(double factor) {
        Physics::setTurbo(instance, factor);
    }


private:
    double power;
    double drag;
    double slipConst;
    double slipFactor;
    bool isPowerInited;
    bool isDragInited;
    double basePower;
    double turboFactor;
    //double prevSpeed;
    //double prevThrottle;
    //double
    PhysicsLog prevLog;
    PhysicsLogs logs;
    vector<AngleLog> angleLogs;

    int countPieceId;
    double sumSpeed;

    static Physics instance;

    static void setTurbo(Physics& p, double factor) {
        p.turboFactor = factor;
        p.power = p.basePower * factor;
    }

    static void logAll(Physics& p) {
        cout << "pieceId avgSpeed maxAngle radius" << endl;
        for (const auto& log : p.angleLogs) {
            cout << log.pieceId << " " <<  log.avgSpeed << " " << log.maxAngle << " " << log.radius << endl;
        }
    }

    static void log(Physics& p, const PhysicsLog& log) {
        if (log.gameTick > 0) {
            if (log.pieceId > p.prevLog.pieceId && p.prevLog.pieceId >= 0) {
                p.angleLogs.push_back(AngleLog{p.prevLog.pieceId, p.sumSpeed / p.countPieceId, p.prevLog.angle, p.prevLog.radius});
                p.sumSpeed = log.speed;
                p.countPieceId = 1;
            } else {
                ++p.countPieceId;
                p.sumSpeed += log.speed;
            }
        }
        //cout << p.prevThrottle << " " << p.prevSpeed << " " << log.speed << endl;
        //cout << "Physics: speed:" << log.speed << " throttle:" << log.throttle << endl;
        if (!p.isPowerInited && p.prevLog.speed == 0 && p.prevLog.throttle == 1) {
            p.basePower = p.power = log.speed;
            p.isPowerInited = true;
            cout << "power " << p.power << endl;
        }
        if (p.isPowerInited /*&& !p.isDragInited &&*/ &&  log.gameTick > 3) {
            if (log.throttle == 1) {
                double d = (p.prevLog.throttle * p.power - (log.speed - p.prevLog.speed)) / log.speed;
                if (!p.isDragInited) {
                    p.drag = d;
                    cout << "drag " << p.drag << " " << p.power << " "<< p.prevLog.throttle << " "<< log.speed << " "<< p.prevLog.speed << " " << endl;
                }
                //else if (d > p.drag * 1.05) {
                //    cout << "Found larger drag: " << d << endl;
                //    p.drag = d;
                //}
            }
            if (!p.isDragInited && (log.throttle != 1 || log.gameTick > 15)) {
                p.isDragInited = true;
                cout << "final drag " << p.drag << endl;
            }
        }
        p.prevLog = log;
    }

    Physics() : power{0.2}, drag{0.02}, slipConst{-125.2243}, slipFactor{395.7098}, isPowerInited{false}, isDragInited{false}, basePower{0.2}, turboFactor{1}, prevLog{}, logs{} {
        int countPieceId = 0;
        double sumSpeed = 0;
    }
};


struct Track2 {
    Lane2s lane2s;
    LaneInfos laneInfos;
    PieceInfos pieceInfos;
    CarInfos carInfos;
    Sectors sectors;
    //int numberOfLaps;

    Track2() {}
    Track2(const Lane2s& lane2s, const LaneInfos& laneInfos, const PieceInfos& pieceInfos, const CarInfos& carInfos, const Sectors& sectors) :
       lane2s{lane2s}, laneInfos{laneInfos}, pieceInfos{pieceInfos}, carInfos{carInfos}, sectors{sectors} {}

    int getCarIdFromColor(const string& color) const {
        int size = carInfos.size();
        for (int i=0; i<size; ++i) {
            if (carInfos[i].color == color) {
                return i;
            }
        }
        cout << "ERROR car color not found: " << color << endl;
        return -1;
    }

    void getTurboPieces();

    //Track(Lanes lanes, Pieces pieces, Cars cars) : lanes{lanes}, pieces{pieces}, cars{cars} {}
};


namespace TrackUtils {
    Track2 createTrackNew(const jsoncons::json& msg);
    //Track createTrack(const jsoncons::json& msg);
    void setDistance(CarPosition2& pos, const Track2& track);
    //void updateCarPositions(Track& track, int deltaGameTicks, const jsoncons::json& msg);
    void updateCarPositions2(Track2& track, int deltaGameTicks, const jsoncons::json& msg);

    double getLength(const PieceInfo& piece, const LaneInfo& lane);
    double getRadius(const PieceInfo& piece, const LaneInfo& lane);

    double calcEndSpeed(double throttle, double speed, double distance);
    //double calcThrottle(double currSpeed, double targetSpeed);
    //double calcDeltaSpeed(double throttle, double speed);
    //double calcNextSpeed(double throttle, double speed, int steps);
    //double calcMaxSpeed(double throttle);
    double calcMaxDeltaAngle(double radius, double speed);
    double calcMaxSpeed(double radius, double maxAngle);
    double calcEstimatedSpeed(double radius, double maxAngle);
}
