#pragma once

#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

//using namespace std;


class Timer {
public:
    Timer() : mSumMicros{0}, mIsRunning{false} {}
    Timer(bool isStarted) : mSumMicros{0}, mIsRunning{false} {
        if (isStarted) {
            start();
        }
    }

    void start() {
        if (mIsRunning) {
            return;
        }
        mIsRunning = true;
        mStartTime = std::chrono::high_resolution_clock::now();
    }

    void reset(bool isStarted) {
        mIsRunning = false;
        mSumMicros = 0;
        if (isStarted) {
            start();
        }
    }

    long stop() {
        if (!mIsRunning) {
            return mSumMicros;
        }
        mIsRunning = false;
        auto stopTime = std::chrono::high_resolution_clock::now();
        mSumMicros += std::chrono::duration_cast<std::chrono::microseconds>(stopTime-mStartTime).count();
        return mSumMicros;
    }

    long getCurrMicros() {
        if (!mIsRunning) {
            return mSumMicros;
        }
        long micros = stop();
        start();
        return micros;
    }

private:
    long mSumMicros;
    bool mIsRunning;
    std::chrono::high_resolution_clock::time_point  mStartTime;
};

struct LogSpeed {
   double speed;
   double maxSpeed;
   double angle;
   double radius;
   double throttle;
   int pieceId;
   double pieceDistance;

   LogSpeed(double speed, double maxSpeed, double angle, double radius, double throttle, int pieceId, double pieceDistance) :
       speed{speed},
       maxSpeed{maxSpeed},
       angle{angle},
       radius{radius},
       throttle{throttle},
       pieceId{pieceId},
       pieceDistance{pieceDistance} {}
};

class Log {
public:
   Log();
   ~Log();
   void log(const LogSpeed& logSpeed);

private:
   std::ofstream mFileSpeed;
   //std::vector<std::ofstream*> files;
};




