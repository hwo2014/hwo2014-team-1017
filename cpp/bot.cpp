#include "bot.h"
//#include <iostream>
#include <stdlib.h>

using namespace std;
using namespace jsoncons;


int lastSitchPiece = 0;
int switchId = 0;
bool isTurboActive = false;

int Bot::isSwitch() {
   int oldSwitch = switchId;
   switchId = 0;
   //if (oldSwitch != 0) {
   //     cout << "## ordered switch " << oldSwitch << endl;
   //}
   return oldSwitch;
}

bool Bot::isTurbo() {
    bool b = isTurboActive;
    if (b) {
        cout << "Activate TURBO!" << endl;
    }
    isTurboActive = false;
    return b;
}

double predictedSpeed = 0;

int prevPieceId = 0;
void Bot::selectAction(int gameTick, Track2& track2) {

    if (!mIsTuboPrepared && Physics::p().isSpeedInitialized()) {
        track2.getTurboPieces();
        mIsTuboPrepared = true;
    }
    //std::cout << "Bot::selectAction" << std::endl;
//    if (!isMaxSpeedsCalculated) {
//        calcMaxSpeeds(track);
//        isMaxSpeedsCalculated = true;
//    }

  //int deltaGameTicks = gameTick - mGameTick;
  mGameTick = gameTick;
  //updateCars(mTrack, deltaGameTicks, msg);


  //cout << "currSpeed=" <<  track.cars[mCarId].getPosition().speed << endl;
  const CarInfo& myCar =  track2.carInfos[mCarId];
  const CarPosition2& pos = myCar.getPosition();
  const PieceInfo& piece = track2.pieceInfos[pos.pieceId];
  double radius = TrackUtils::getRadius(piece, track2.laneInfos[pos.laneId]);
  //double radius = track.pieces[pos.pieceId].radius;
  if (piece.turn == Turn::left) {
      radius = -radius;
  }


  //int currLap = pos.lap + (pos.pieceId >= 27 ? 1 : 0);
  //mThrottle = track2.lane2s[pos.laneId].speedLimitMax.getThrottleAtDistance(pos.totalDistance, pos.speed);

  //double nextDistance = pos.totalDistance + pos.speed;
  double maxSpeed = 5.5 + pos.lap / 2.0; // todo: use speed to test params.
  //if (pos.lap >= 2) {
  maxSpeed = track2.lane2s[pos.laneId].speedLimit.getLimitAtDistance(pos.pieceId, pos.pieceDistance, pos.speed, (pos.lap + 1) == mNumberOfLaps);
  //maxSpeed /= 2;
  //}
  //if (pos.angle > 50) {
  //    maxSpeed = maxSpeed = 0;
  //} else
  if (pos.angle > 53) {
      maxSpeed = 0;
  }


  //if (maxSpeed == 0) {
  //  cout << "maxspeed=" << maxSpeed << endl;
  //}
  if (!track2.carInfos[mCarId].isCrashed) {
    mLog.log(LogSpeed{pos.speed, maxSpeed, pos.angle, radius, mThrottle, pos.pieceId, pos.pieceDistance});
    double expectedAngle = Physics::p().maxDeltaAngle(radius, pos.speed);
    double angleFactor = expectedAngle > 10 ? (abs(pos.angle) / expectedAngle) : 1;
    track2.lane2s[pos.laneId].speedLimit.addSample(pos.pieceId, pos.pieceDistance, SpeedPoint{pos.speed, pos.angle, angleFactor});
  }

  //if (pos.pieceId < 38 && pos.pieceId >= 35) {
  //    maxSpeed = 100;
  //}
  //double totalDistance = pos.totalDistance;
  //double sumDistance = 0;
  //int i = 0;
  //double maxSpeed = 10;

//  for (const auto& segment : track.segments) {
//      sumDistance += segment.length;
//      if (sumDistance >= totalDistance) {
//          maxSpeed = 6; //segment.maxSpeed;
//          break;
//      }
//      ++i;
//  }
//  if (pos.angle > 45) {
//      maxSpeed = 5;
//  }

  //double deltaSpeed = maxSpeed - pos.speed;



  mThrottle = gameTick < 5 ? 1 : Physics::p().adaptThrottle(pos.speed, maxSpeed);
  //mThrottle = gameTick < 20 ? 0.2 : 0.6;
  predictedSpeed = pos.speed + Physics::p().calcDeltaSpeed(pos.speed, mThrottle);

  if (!mIsCrashed) {
      ///////cout << "angle=" << pos.angle << ", totalDistance=" << pos.totalDistance << ", piece=" << pos.pieceId << ", speed=" << pos.speed << ", maxSpeed=" << maxSpeed << ", throttle=" << mThrottle << endl;
      ///////cout << pos.totalDistance << " " << pos.speed << " " << pos.pieceId << endl;
      //cout << pos.angle << " " << pos.speed << " " << maxSpeed << " " << mThrottle << " " << pos.totalDistance  << " " << pos.laneId << ":" << pos.pieceId  << endl;
      Physics::log(PhysicsLog{gameTick, piece.id, mThrottle, radius, pos.speed, pos.angle}, track2);
  }

  //double maxSpeed = 0;


  //cout << "oldinit " << myCar.getPositionOld().isInited() << " new inti" << myCar.getPositionOld().isInited() << " eqw" << (myCar.getPositionOld().pieceId != pos.pieceId) << endl;
  if ( myCar.getPositionOld().isInited()) {
  if (!myCar.getPositionOld().isInited() || (myCar.getPositionOld().isInited() && myCar.getPositionOld().pieceId != pos.pieceId)) {
      //bool isLastLap = pos.lap + 1 == mNumberOfLaps;
      //cout << "*** FIX LIMITS *** " << isLastLap << endl;
      for (auto& lane : track2.lane2s) {
          lane.speedLimit.fix();
      }
  }
  }

  const Sector& sector = track2.sectors[pos.sectorId];
  int hash = pos.lap * track2.pieceInfos.size() + pos.pieceId;
  //cout << "sectorId=" << pos.sectorId << " pos.pieceDistance=" << pos.pieceDistance << ", piece.length/2=" << (piece.length / 2);
  //cout << " sector.pieceId=" << sector.pieceId << " %=" <<  ((pos.pieceId + 1) % track2.pieceInfos.size())  << endl;


  if (hash != lastSwitchHash && sector.pieceId == ((pos.pieceId + 1) % track2.pieceInfos.size())) {
     if (pos.pieceDistance >= (piece.length / 2)) {
        lastSwitchHash = hash;
        int sectorIndex = selectNextSection(track2, pos);
        const Sector& nextSector = track2.sectors[sectorIndex];
        //int index = rand() % sector.nextSectors.size();
        //const Sector& nextSector = track2.sectors[sector.nextSectors[index]];
        //const Sector& nextSector = track2.sectors[sector.nextSectors[sector.closestIndex]];
        if (sector.laneId != nextSector.laneId) {
            switchId = sector.laneId > nextSector.laneId ? 1 : 2;
        }
     }
  }

  if (mIsTurboAvailable) {
      int nextPieceId = (pos.pieceId + 1) % track2.pieceInfos.size();
      bool isChangingPiece = (pos.pieceDistance + pos.speed) > piece.length;
      if (piece.isTurbo || (isChangingPiece && track2.pieceInfos[nextPieceId].isTurbo)) {
        cout << "released turbo" << endl;
        isTurboActive = true;
        mIsTurboAvailable = false;
        return;
      }
  }

//  switchId = 0;
//  if ((lastSitchPiece != pos.pieceId) && (pos.pieceId == 6  || pos.pieceId == 15 || (pos.pieceId == 1 && pos.lap == 0))) {
//      lastSitchPiece = pos.pieceId;
//      switchId = pos.pieceId == 6 ? 1 : 2;
//  }
  // todo: throttle = 0 or maxSpeed / 10;

  //mThrottle = calcThrottle(gameTick);
  //std::cout << "Bot::selectAction done" << std::endl;
}

bool isSectorBetter(double currDistanceToGoal, double currBlockTime, double bestDistanceToGoal, double bestBlockTime) {
    if (currBlockTime > 0 && bestBlockTime > 0) {
        // None blocked. Select shortest path.
        return currDistanceToGoal < bestDistanceToGoal;
    }
    // At least one block. Select least blocked.
    return currBlockTime > bestBlockTime;
}

int Bot::selectNextSection(const Track2& track, const CarPosition2& myPosition) {
    vector<double> blockTimes;
    const Sector& mySector = track.sectors[myPosition.sectorId];
    for (int sectorId : mySector.nextSectors) {
        const auto& currSector = track.sectors[sectorId];
        double myTimeLeave = (track.sectors[myPosition.sectorId].endDistance - myPosition.totalDistance + currSector.deltaDistance) / (myPosition.speed + 0.01);
        double blockTime = 10000;
        for (const auto& car : track.carInfos) {
            if (car.color == mCarColor) {
                continue;
            }
            const auto& pos = car.position;
            if (pos.sectorId == sectorId) {
                cout << "Found car in next sector" << endl;
                double expectedTimeLeave = (currSector.endDistance - pos.totalDistance) / (pos.speed + 0.01);
                double deltaTime = myTimeLeave - expectedTimeLeave;
                cout << "Car on next sector: timeDelta=" << deltaTime << endl;
                if (deltaTime < blockTime) {
                    blockTime = deltaTime;
                }
            }
        }
        blockTimes.push_back(blockTime);
    }

    int bestIndex = -1;
    double bestDistanceToGoal = 0;
    double bestBlockTime = 0;
    for (int i=0; i<mySector.nextSectors.size(); ++i) {
        const auto& currSector = track.sectors[mySector.nextSectors[i]];
        bool isBetter = isSectorBetter(currSector.distanceToGoal, blockTimes[i], bestDistanceToGoal, bestBlockTime);
        if (bestIndex < 0 || isBetter) {
            bestDistanceToGoal = currSector.distanceToGoal;
            bestBlockTime = blockTimes[i];
            bestIndex = i;
        }
    }
    //int index = rand() % currSector.nextSectors.size();
    return mySector.nextSectors[bestIndex];
}

void Bot::onGameEnd(const Track2& track) {
    cout << "Bot.onGameEnd()" << endl;
    for (const auto& lane : track.lane2s) {
        lane.speedLimit.log();
    }
    //Physics::LogAll();
}

void Bot::onTurboAvailable(const jsoncons::json& msg) {
    cout << " mIsTurboAvailable = true" << endl;
    mIsTurboAvailable = true;
}

void Bot::onYourCar(const json& msg) {
    std::cout << "onYourCar:" << msg.to_string() << std::endl;
    mCarColor = msg["color"].as<std::string>();
    cout << "onYourCar:color=" << mCarColor << endl;
}

void Bot::onGameInit(const Track2& track2, bool isQuickRace, int numberOfLaps) {

    //mTrack = createTrack(msg);
    //mTrack.log();
    mIsQuickRace = isQuickRace;
    mNumberOfLaps = numberOfLaps;
    mCarId = track2.getCarIdFromColor(mCarColor);
    cout << "Bot.OnGameInit. color" << mCarColor << ", mId=" << mCarId << endl;
}

//const Track& Bot::getTrack() {
//   return mTrack;
//}

double Bot::getThrottle() {
   //mThrottle = calcThrottle(0);
   return mThrottle;
}
