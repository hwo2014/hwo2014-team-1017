#include "connection_dummy.h"
#include <iostream>
#include <string>


jsoncons::json createJoin();
jsoncons::json createYourCar();
jsoncons::json createGameInit();
jsoncons::json createGameStart();
jsoncons::json createCarPositions(int pieceId, double distance, int gameTick);
jsoncons::json createGameEnd();

const int STATE_JOIN = 0;
const int STATE_YOUR_CAR = 1;
const int STATE_GAME_INIT = 2;
const int STATE_GAME_START = 3;
const int STATE_CAR_POSITIONS = 4;
const int STATE_GAME_END = 5;

jsoncons::json dummy_connection::receive_response()
{
    //std::cout << "curr state=" << state << std::endl;
    jsoncons::json msg;
    switch (state) {
    case STATE_JOIN:
        msg = createJoin();
        break;
    case STATE_YOUR_CAR:
        msg = createYourCar();
        break;
    case STATE_GAME_INIT:
        msg = createGameInit();
        break;
    case STATE_GAME_START:
        msg = createGameStart();
        break;
    case STATE_CAR_POSITIONS:
        {
             //double force = 110.0 * throttle - 2 * speed;
             //double forceFriction = 10;
             //if (speed > 0) {
             //   force -= forceFriction;
             //}
             //double a = force / 33.0;
             double deltaSpeed = throttle / 5 - speed * 0.019816627;
             speed += deltaSpeed;
             if (speed < 0) {
                speed = 0;
             }
             distance += speed;
             double pieceLength = TrackUtils::getLength(mTrack.pieceInfos[pieceId], mTrack.laneInfos[0]);
             if (distance > pieceLength) {
                 distance -= pieceLength;
                 ++pieceId;
                 std::cout << "New PieceId " << pieceId << ", distance=" << distance << std::endl;
                 if (pieceId >= mTrack.pieceInfos.size()) {
                     pieceId = 0;
                     ++lap;
                     int ticksForLap = gameTick - ticksLapStart;
                     if (ticksForLap <= bestLap) {
                         bestLap = ticksForLap;
                     }
                     std::cout << "### Lap(" << gameTick << ") : lap " << ticksForLap << ", best " << bestLap << std::endl;
                     ticksLapStart = gameTick;
                 }
             }

             //double angle = -125.2243 + 395.7098 * speed * speed / radius;

             //std::cout << "CAR acc=" << a << ", speed=" << speed << ", dist=" << distance << std::endl;
             msg = createCarPositions(pieceId, distance, gameTick);
             ++gameTick;
        }
        break;
    case STATE_GAME_END:
        cout << "dummy_connection createGameEnd" << endl;
        msg = createGameEnd();
        break;
    }
    if (state != STATE_CAR_POSITIONS || lap >= 1) {
       ++state;
    }

    //std::cout << "next state=" << state << std::endl;
    return msg;
}

void dummy_connection::setTrack(const Track2& track) {
    mTrack = track;
}

void dummy_connection::send_requests(const std::vector<jsoncons::json>& msgs)
{
   const auto& obj = msgs[0];
   //std::cout << "sending request: " << obj.to_string() << std::endl;
   if (obj["msgType"].as<std::string>() == "throttle") {
      throttle = obj["data"].as<double>();
      //std::cout << "#### Throttle=" << throttle << std::endl;
   }
    ++countMessages;

}

jsoncons::json createJoin() {
    return jsoncons::json::parse_string("{\"msgType\": \"join\", \"data\": {\
        \"name\": \"Forrest\",\
        \"key\": \"UEWJBVNHDS\"\
    }}");
}

jsoncons::json createYourCar() {
    return jsoncons::json::parse_string("{\"msgType\": \"yourCar\", \"data\": {\
        \"name\": \"Schumacher\",\
        \"color\": \"red\"\
        }}");
}

jsoncons::json createGameStart() {
    return jsoncons::json::parse_string("{\"msgType\": \"gameStart\", \"data\": null}");
}

jsoncons::json createCarPositions(int pieceId, double distance, int gameTick) {
   std::string s = "{\"msgType\": \"carPositions\", \"data\": [\
   {\
     \"id\": {\
       \"name\": \"Schumacher\",\
       \"color\": \"red\"\
     },\
     \"angle\": 0.0,\
     \"piecePosition\": {\
       \"pieceIndex\": ";
   //s.append("pieceIndex\": ");
   s.append(std::to_string(pieceId));
   s.append(",\"inPieceDistance\": ");
   s.append(std::to_string(distance));
   s.append(",\"lane\": {\
         \"startLaneIndex\": 0,\
         \"endLaneIndex\": 0\
       },\
       \"lap\": 0\
     }\
   }\
 ], \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": ");
 s.append(std::to_string(gameTick));
 s.append("}");
 //std::cout << "STRING:" << s << std::endl;
   auto obj = jsoncons::json::parse_string(s);
   /************
   auto obj = jsoncons::json::parse_string("{\"msgType\": \"carPositions\", \"data\": [\
  {\
    \"id\": {\
      \"name\": \"Schumacher\",\
      \"color\": \"red\"\
    },\
    \"angle\": 0.0,\
    \"piecePosition\": {\
      \"pieceIndex\": " + pieceId + ",\"inPieceDistance\": " + distance + ",\
      \"lane\": {\
        \"startLaneIndex\": 0,\
        \"endLaneIndex\": 0\
      },\
      \"lap\": 0\
    }\
  },\
  {\
    \"id\": {\
      \"name\": \"Rosberg\",\
      \"color\": \"blue\"\
    },\
    \"angle\": 0.0,\
    \"piecePosition\": {\
      \"pieceIndex\": 0,\
      \"inPieceDistance\": 20.0,\
      \"lane\": {\
        \"startLaneIndex\": 1,\
        \"endLaneIndex\": 1\
      },\
      \"lap\": 0\
    }\
  }\
], \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 0}");
************/
  //std::cout << "CARPOS:" << obj.to_string() << std::endl;
  return obj;
}

jsoncons::json createGameInit() {
    return jsoncons::json::parse_file("/localhome/mtobiass/proj/Forrest/tracks/keimola.json");
    //return jsoncons::json::parse_file("/home/mtobiass/Downloads/keimola.json");

    return jsoncons::json::parse_string("{\"msgType\": \"gameInit\", \"data\": {\
                                        \"race\": {\
                                          \"track\": {\
                                            \"id\": \"indianapolis\",\
                                            \"name\": \"Indianapolis\",\
                                            \"pieces\": [\
                                              {\"length\": 100.0},\
                                              {\
                                                \"length\": 100.0,\
                                                \"switch\": true\
                                              },\
                                              {\
                                                \"radius\": 160,\
                                                \"angle\": 48.0\
                                              },\
                                              {\
                                                  \"radius\": 100,\
                                                  \"angle\": 45.0\
                                              },\
                                                {\
                                                    \"radius\": 180,\
                                                    \"angle\": 45.0\
                                                }\
                                            ],\
                                            \"lanes\": [\
                                              {\
                                                \"distanceFromCenter\": 0,\
                                                \"index\": 0\
                                              },\
                                              {\
                                                \"distanceFromCenter\": 20,\
                                                \"index\": 1\
                                              }\
                                            ],\
                                            \"startingPoint\": {\
                                              \"position\": {\
                                                \"x\": -340.0,\
                                                \"y\": -96.0\
                                              },\
                                              \"angle\": 90.0\
                                            }\
                                          },\
                                          \"cars\": [\
                                            {\
                                              \"id\": {\
                                                \"name\": \"Schumacher\",\
                                                \"color\": \"red\"\
                                              },\
                                              \"dimensions\": {\
                                                \"length\": 40.0,\
                                                \"width\": 20.0,\
                                                \"guideFlagPosition\": 10.0\
                                              }\
                                            },\
                                            {\
                                              \"id\": {\
                                                \"name\": \"Rosberg\",\
                                                \"color\": \"blue\"\
                                              },\
                                              \"dimensions\": {\
                                                \"length\": 40.0,\
                                                \"width\": 20.0,\
                                                \"guideFlagPosition\": 10.0\
                                              }\
                                            }\
                                          ],\
                                          \"raceSession\": {\
                                            \"laps\": 3,\
                                            \"maxLapTimeMs\": 30000,\
                                            \"quickRace\": true\
                                          }\
                                        }\
    }}");

}

jsoncons::json createGameEnd() {
    //return jsoncons::json::parse_string("{\"msgType\": \"gameStart\", \"data\": null}");
    return jsoncons::json::parse_string("{\"msgType\": \"gameEnd\", \"data\": null}");
}
