#include "game_logic.h"
#include "protocol.h"
#include <time.h>

using namespace hwo_protocol;

game_logic::game_logic()
  : mTrack{}, action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "spawn", &game_logic::on_spawn },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "turboAvailable", &game_logic::on_turbo_available },
      { "turboStart", &game_logic::on_turbo_start},
      { "turboEnd", &game_logic::on_turbo_end },
      { "lapFinished", &game_logic::on_ignore }

    }
{
  mGameTick = 0;
  mGameTickDelta = 0;
  myCarColor = "";
  isCrashed = false;
  isQuickRace = false;
  numberOfLaps = 3;
  srand (time(NULL));
}

const Track2& game_logic::getTrack() {
    return mTrack;
}


game_logic::msg_vector game_logic::get_reply()
{
    //cout << "sending reply" << endl;
    if (bot.isTurbo()) {
        return { make_turbo() };
    }
    double throttle = bot.getThrottle();
    int switchIndex = bot.isSwitch();
    if (switchIndex != 0) {
        return { make_switch( switchIndex == 1) };
    } else {
        return { make_throttle(throttle) };
    }
}

bool game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  if (!msg.has_member("data")) {
      cout << "no data member in message" << endl;
      return false;
  }
  const auto& data = msg["data"];

  bool sendReply = (msg_type == "gameStart" || (msg_type == "carPositions" && msg.has_member("gameTick")));
  //cout << "got message:" << msg_type << endl;

  if (msg_type == "carPositions" && msg.has_member("gameTick")) {
      int tick = msg["gameTick"].as<int>();
      //cout << "Got tick[" << tick << "] in msg " << msg_type << endl;
      mGameTickDelta = tick - mGameTick;
      if (mGameTickDelta != 1) {
          std::cout << "old tick=" << mGameTick << " msg=" << msg.to_string() << std::endl;
      }
      mGameTick = tick;
  }
//  if (msg_type == "spawn" || msg_type == "crash" || mGameTick < 10) {
//      std::cout << "[currTick=" << mGameTick << "] " << msg.to_string()  << std::endl;
//  }

  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
     //std::cout << "msg data:" << data.to_string() << endl;
    (action_it->second)(this, data);
  }
  else
  {
    std::cout << "?? Unknown message type: " << msg.to_string() << std::endl;
    //return { make_ping() };
  }

  return sendReply; //msg_type == "carPositions";
}

void game_logic::on_ignore(const jsoncons::json& data)
{
}

void game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  //return { make_ping() };
}

void game_logic::on_turbo_available(const jsoncons::json& data)
{
  mTurboFactor = data["turboFactor"].as<double>();
  std::cout << "Turbo! " << data.to_string() << std::endl;
  cout << "mTurboFactor=" << mTurboFactor << endl;
  bot.onTurboAvailable(data);
}

void game_logic::on_turbo_start(const jsoncons::json& data)
{
  if (myCarColor != data["color"].as<string>()) {
      return;
  }
  std::cout << "Turbo start " << data.to_string() << std::endl;
  Physics::setTurbo(mTurboFactor);
}

void game_logic::on_turbo_end(const jsoncons::json& data)
{
  if (myCarColor != data["color"].as<string>()) {
     return;
  }
  std::cout << "Turbo end" << data.to_string() << std::endl;
  Physics::setTurbo(1);
}


void game_logic::on_your_car(const jsoncons::json& data)
{
    myCarColor = data["color"].as<string>();
    std::cout << "Your car=" << myCarColor << std::endl;
    bot.onYourCar(data);
  //return { make_ping() };
}

void game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "Game Init" << std::endl;
  if (mTrack.laneInfos.size() == 0) {
    cout << "CREATE NEW TRACK" << endl;
    mTrack = TrackUtils::createTrackNew(data);
  } else {
      cout << "REUSE TRACK" << endl;
  }
  if (data["race"]["raceSession"].has_member("laps")) {
    numberOfLaps = data["race"]["raceSession"]["laps"].as<int>();
  } else if (data["race"]["raceSession"].has_member("lap")) {
     numberOfLaps = data["race"]["raceSession"]["lap"].as<int>();
  } else {
      cout << "laps not found: " << data.to_string();
      numberOfLaps = 5;
  }
  //isQuickRace = data["race"]["raceSession"]["quickRace"].as<bool>();
  cout << "numberOfLaps=" << numberOfLaps << endl;
  bot.onGameInit(mTrack, isQuickRace, numberOfLaps);
  //return { make_ping() };
}

void game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  //return { make_throttle(bot.getThrottle()) };
}

void game_logic::on_car_positions(const jsoncons::json& data)
{
  //cout << "mGameTick=" << mGameTick;
  //if (mGameTick % 10 == 0) {
  //   mIsAccelerating = !mIsAccelerating;
  //}
  //TrackUtils::updateCarPositions(mTrack, mGameTickDelta, data);
  //cout << "CARPOS:" << data.to_string() << endl;
  TrackUtils::updateCarPositions2(mTrack, mGameTickDelta, data);
  //cout << "SPEED1: " << mTrack.cars[0].getPosition().speed << endl;
  bot.selectAction(mGameTick, mTrack);

  //double throttle = bot.getThrottle();
  //return { make_throttle(throttle) };
}

void game_logic::on_crash(const jsoncons::json& data)
{
  isCrashed = true;
  std::cout << "Crashed  " << data["color"].as<std::string>() << std::endl;
  bot.setCrashed(true);
  //return { make_ping() };
}

void game_logic::on_spawn(const jsoncons::json& data)
{
  isCrashed = false;
  std::cout << "Spawned " << data["color"].as<std::string>() << std::endl;
  bot.setCrashed(false);
  //return { make_ping() };
}

void game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  bot.onGameEnd(mTrack);
  //return { make_ping() };
}

void game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  //return { make_ping() };
}
