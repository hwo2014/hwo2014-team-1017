#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H


#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "bot.h"


class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  bool react(const jsoncons::json& msg);
  const Track2& getTrack();

  msg_vector get_reply();

private:
  Bot bot;
  //Track mTrack;
  Track2 mTrack;
  int mGameTick;
  int mGameTickDelta;
  string myCarColor;
  double mTurboFactor;
  bool isQuickRace;
  int numberOfLaps;
  //bool mIsAccelerating;
  typedef std::function<void(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;


  bool isCrashed;

  void on_join(const jsoncons::json& data);
  void on_your_car(const jsoncons::json& data);
  void on_game_init(const jsoncons::json& data);
  void on_game_start(const jsoncons::json& data);
  void on_car_positions(const jsoncons::json& data);
  void on_crash(const jsoncons::json& data);
  void on_spawn(const jsoncons::json& data);
  void on_game_end(const jsoncons::json& data);
  void on_error(const jsoncons::json& data);
  void on_turbo_available(const jsoncons::json& data);
  void on_turbo_start(const jsoncons::json& data);
  void on_turbo_end(const jsoncons::json& data);
  void on_ignore(const jsoncons::json& data);

};

#endif
