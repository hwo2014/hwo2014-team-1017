#include "util.h"

using namespace std;

const int FILE_SPEED = 0;

void Log::log(const LogSpeed& d) {
   mFileSpeed << d.speed << ";" << d.maxSpeed << ";" << d.angle << ";" << d.radius << ";" << d.throttle << ";" << d.pieceId << ";" << d.pieceDistance << endl;
   mFileSpeed.flush();
   //cout << "LOG" << d.maxSpeed << endl;
}

Log::Log() : mFileSpeed{"speed.dat"} {
   mFileSpeed << "speed;maxspeed;angle;radius;throttle;pieceId;pieceDistance" << endl;
}

Log::~Log() {
   mFileSpeed << "close" << endl;
   mFileSpeed.close();
}
