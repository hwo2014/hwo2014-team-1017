#include "track.h"
#include <math.h>
#include <algorithm>

using namespace std;
using namespace jsoncons;

//double Physics::power = 0.2;
//double Physics::drag = 0.019816627;
//double Physics::slipConst = 0.0;
//double Physics::slipFactor = 0.0;
//bool Physics::isPowerInited = false;
//bool Physics::isDragInited = false;
const double maxBucketLength = 10;

Physics Physics::instance;

CarInfo::CarInfo(int id, const string& color) : id{id}, color{color}, isCrashed{false}, position{}, positionOld{}  {}

const CarPosition2& CarInfo::getPosition() const { return position; }
const CarPosition2& CarInfo::getPositionOld() const { return positionOld; }

void CarInfo::updatePosition(const Track2& track, const CarPosition2& newPos, int deltaGameTicks) {
    double speed = 0;
    if (position.isInited() && deltaGameTicks > 0) {
        if (newPos.lap == position.lap) {
            speed = (newPos.totalDistance - position.totalDistance) / deltaGameTicks;
        } else {
            const auto& pieceLengths = track.lane2s[newPos.laneId].pieceLengths;
            double deltaDist = newPos.totalDistance + pieceLengths[pieceLengths.size()-1] - position.totalDistance;
            speed = deltaDist / deltaGameTicks;
        }
    }
    positionOld = position;
    position = newPos;
    position.speed = speed;
}

/////////////////////////////////////////////////////////////////////////////


//SpeedLimit::SpeedLimit(int bucketsPerPiece, const vector<SpeedBucket>& buckets, const vector<double>& bucketLengths) :
//    bucketsPerPiece{bucketsPerPiece}, buckets{buckets}, bucketLengths{bucketLengths} {
//}


void SpeedLimit::addSample(int pieceId, double pieceDistance, const SpeedPoint& point) {

    //if (point.angle < 1) {
    //    return;
    //}
    SpeedBucket& bucket = getBucket(pieceId, pieceDistance);
    if (!bucket.isTurn) {
        return;
    }
    bucket.addPoint(point);

//    double expectedAngle = Physics::p().maxDeltaAngle(bucket.radius, point.speed);
//    if (expectedAngle > 10) {
//        //cout << "expectedangle=" << expectedAngle << endl;
//        double factor = max(0.1, abs(point.angle) / expectedAngle);
//        if (bucket.angleFactor == 1) {
//            bucket.angleFactor = factor;
//        } else {
//            factor = max(bucket.angleFactor, factor);
//        }
//        if (factor != bucket.angleFactor) {
//            bucket.angleFactor = factor;
//            double maxSpeed = Physics::p().calcMaxSpeed(bucket.radius, 55 / bucket.angleFactor);
//            //cout << "new angleFactor " << factor << ", maxSpeed " << bucket.maxSpeed << " -> " << maxSpeed << endl;
//            bucket.maxSpeed = maxSpeed;
//        }
//    }
}

SpeedBucket& SpeedLimit::getBucket(int pieceId, double pieceDistance) {
    for (auto& bucket : buckets) {
        if (bucket.pieceId == pieceId) {
            double start = bucket.pieceDistance;
            double end = start + bucket.length;
            if (pieceDistance >= start && pieceDistance <= end) {
                return bucket;
            }
        }
    }
}


//double SpeedLimit::getLimitAtDistance(int pieceId, double pieceDistance) {
//    const SpeedBucket& bucket = getBucket(pieceId, pieceDistance);
//    return bucket.maxSpeedFuture;
//}

//double SpeedLimit::getLimitAtDistance(int pieceId, double pieceDistance) {
//    const SpeedBucket& bucket = getBucket(pieceId, pieceDistance);
//    //return bucket.maxSpeedFuture;
//    //double speedLimit = bucket.maxSpeedFuture;
//    double distance = bucket.length - pieceDistance;
//    for (int i=1; i<100; ++i) {

//    }
//    return bucket.maxSpeedFuture;
//}

double SpeedLimit::getLimitAtDistance(int pieceId, double pieceDistance, double currSpeed, bool isLastLap) {
    const SpeedBucket& bucket = getBucket(pieceId, pieceDistance);
    //return bucket.maxSpeedFuture;
    //double speedLimit = bucket.maxSpeedFuture;
    double distance = bucket.length - pieceDistance - currSpeed;
    for (int i=1; i<100; ++i) {
        int bucketIndex = (bucket.id + i) % buckets.size();
        if (bucketIndex == 0 && isLastLap) {
            break;
        }
        const SpeedBucket& currBucket = buckets[bucketIndex];
        distance += currBucket.length;
        double lowestSpeed = distance <= 0 ? currSpeed : TrackUtils::calcEndSpeed(0, currSpeed, distance);
        if (lowestSpeed < 1 || currSpeed < 1) {
            break;
        }
        if (lowestSpeed >= currBucket.maxSpeed) {
            return 0;
        }
    }
    if (currSpeed <= bucket.length - pieceDistance) {
        return bucket.maxSpeed;
    } else {
        return buckets[(bucket.id + 1) % buckets.size()].maxSpeed;
    }
    //double limitSpeed = min(bucket.maxSpeed, bucket[(bucket.id + i) % buckets.size()]);
}



//void SpeedLimit::fix(const CarPosition2 &pos, bool isLastLap) {
//    if (isLastLap) {
//         for (int i=0; i<buckets.size(); ++i) {
//             if (buckets[i].pieceId < pos.pieceId) {
//                 buckets[i].maxSpeed = 1000;
//                 buckets[i].maxSpeedFuture = 1000;
//             }
//         }
//    }
//    if (isLastLap && pos.pieceId == 35) {
//        cout << buckets[0].maxSpeed << " " << buckets[0].maxSpeedFuture << endl;
//    }
//    fix();
//    if (isLastLap && pos.pieceId == 35) {
//        cout << buckets[0].maxSpeed << " " << buckets[0].maxSpeedFuture << endl;
//    }

//    if (isLastLap && pos.pieceId == 35) {
//        cout << "LAST LOG:" << endl;
//        log();
//    }
//}

void SpeedLimit::fix() {
//    for (int i=0; i<buckets.size(); ++i) {
//        double angleFactor = 0;
//        for (const auto& p : buckets[i].speedPoints) {
//            angleFactor = max(angleFactor, p.angleFactor);
//        }
//        if (angleFactor == 0 || angleFactor > 0.5) {
//            angleFactor = 1;
//        }
//        angleFactor *= 1.5;
//        angleFactor = min(1.0, angleFactor);
//        //angleFactor = 1;
//        //cout << "anglefacotr " << angleFactor << endl;
//        buckets[i].maxSpeed = Physics::p().calcMaxSpeed(buckets[i].radius, 55 / max(angleFactor, 0.2));
//        buckets[i].maxSpeedFuture = buckets[i].maxSpeed;
//    }

    int currIndex = -1;
    double minSpeed = 100000;
    for (int i=0; i<buckets.size(); ++i) {
        if (currIndex < 0 || buckets[i].maxSpeed < minSpeed) {
            currIndex = i;
            minSpeed = buckets[i].maxSpeed;
        }
    }

    int prevIndex = currIndex;
    for (int i=0; i<buckets.size(); ++i) {
        //currIndex = currIndex >= 0 ? currIndex - 1 : buckets.size() - 1;
        //int nextIndex = currIndex > 0 ? currIndex - 1 : buckets.size() - 1;
        --currIndex;
        if (currIndex < 0) {
            currIndex = buckets.size() - 1;
        }
        //double t = (buckets[currIndex].length + buckets[prevIndex].length) / 2;
        //double dv = -0.02 * t * buckets[currIndex].maxSpeedFuture;
        //double limit = max(buckets[currIndex].maxSpeedFuture + dv, buckets[prevIndex].maxSpeedFuture);
        //buckets[currIndex].maxSpeedFuture = min(limit, buckets[currIndex].maxSpeed);

        double deltaSpeed = TrackUtils::calcEndSpeed(0, buckets[prevIndex].maxSpeedFuture, buckets[currIndex].length) - buckets[prevIndex].maxSpeedFuture;
        if (deltaSpeed >= 0) {
            cout << "deltaSpeed=" << deltaSpeed << " prev=" << buckets[prevIndex].maxSpeedFuture << " newLimit=" << min(buckets[prevIndex].maxSpeedFuture + deltaSpeed, buckets[currIndex].maxSpeed) << endl;
        }
        //double limit = 2 * buckets[prevIndex].maxSpeedFuture - TrackUtils::calcEndSpeed(0, buckets[prevIndex].maxSpeedFuture, buckets[currIndex].length);
        //if (limit < buckets[prevIndex].maxSpeedFuture) {
        //    cout << "## limit=" << limit << " " << buckets[prevIndex].maxSpeedFuture << endl;
        //}
        buckets[currIndex].maxSpeedFuture = min(buckets[prevIndex].maxSpeedFuture - deltaSpeed, buckets[currIndex].maxSpeed);
        //if (isLastLap && bucket.pieceId < carPieceId) {
        //
        //}
        prevIndex = currIndex;
    }
}

void update() {
}


void SpeedLimit::logLimit() const {
    cout << "speedLimit log" << endl;
    cout << "bucket pieceId pieceDist distance length radius max future" << endl;
    int i=0;
    double distanceFromStart = 0;
    for (const auto& b : buckets) {
        cout << i << " " << b.pieceId << " " << b.pieceDistance << " " << distanceFromStart << " " << b.length << " " << b.radius << " " << b.maxSpeed << " " << b.maxSpeedFuture << endl;
        distanceFromStart += b.length;
        ++i;
    }
}


void logLinearFunction(const SpeedBucket& bucket) {
    if (bucket.speedPoints.empty() || !bucket.isTurn) {
        return;
    }
    double avgSpeed = 0;
    double avgSquaredSpeed = 0;
    double avgAngle = 0;
    //double sumSquaredAngle = 0;
    double avgSpeedAngle = 0;
    for (const auto& p : bucket.speedPoints) {
        double speed = p.speed * p.speed;
        avgSpeed += speed;
        avgSquaredSpeed += speed * speed;
        avgAngle += p.angle;
        //sumSquaredAngle += p.angle * p.angle;
        avgSpeedAngle += speed * p.angle;
    }
    double n = bucket.speedPoints.size();
    avgSpeed /= n;
    avgSquaredSpeed /= n;
    avgAngle /= n;
    avgSpeedAngle /= n;

    if (abs(avgSpeed * avgSpeed - avgSquaredSpeed) < 0.00000001) {
        cout << "infinite slope " << (avgSpeed * avgSpeed - avgSquaredSpeed) << endl;
        return;
    }
    double slope = (avgSpeed * avgAngle - avgSpeedAngle) / (avgSpeed * avgSpeed - avgSquaredSpeed);
    double initial = avgAngle - slope * avgSpeed;

//    double n = bucket.speedPoints.size();
//    double ssxx = sumSquaredSpeed - sumSpeed * sumSpeed / n;
//    //double ssyy = sumSquaredAngle - sumAngle * sumAngle / n;
//    double ssxy = sumSpeedAngle - sumSpeed * sumAngle / n;

//    if (ssxx == 0) {
//        cout << "ssxx == 0" << endl;
//        return;
//    }
//    double initial = ssxy / ssxx;
//    double slope = sumAngle / n - initial * sumSpeed / n;
    double sumError = 0;
    for (const auto& p : bucket.speedPoints) {
        double estimate = initial + slope * p.speed * p.speed;
        double error = estimate - p.angle;
        sumError += error * error;
    }
    //cout << bucket.id << " " << initial << " " << slope << " " << (sumError / n) << " " << avgSpeed << " " << avgAngle << endl;
}

void SpeedLimit::log() const {
    cout << "speedLimit log" << endl;
    cout << "bucket radius maxspeed futurespeed speed angle angleFactor" << endl;
    //int i=0;
    for (const auto& b : buckets) {
        //if (!b.isTurn) {
        //    continue;
        //}
        const auto& points = b.speedPoints;
        for (const auto& p : points) {
            cout << b.id << " " << b.radius << " " << b.maxSpeed << " " << b.maxSpeedFuture << " " << p.speed << " " << p.angle << " " << p.angleFactor << endl;
        }
        //++i;
    }
    //cout << "bucket initial slope avgError avgSpeed avgAngle" << endl;
    //for (const auto& b : buckets) {
    //    logLinearFunction(b);
   // }
}

void Track2::getTurboPieces() {
    vector<int> counters;
    vector<int> pieceIds;
    const vector<SpeedBucket>& buckets = lane2s[0].speedLimit.buckets;
    int prevPieceId = -1;
    int maxId = -1;
    int maxCounter = 0;
    for (int i=0; i<buckets.size(); ++i) {
        const auto& currBucket = buckets[i];
        if (currBucket.pieceId == prevPieceId) {
            continue;
        }
        prevPieceId = currBucket.pieceId;
        double speed = currBucket.maxSpeed;
        int counter = 1;
        while (counter < 300) {
            int bucketIndex = (i + counter) % buckets.size();
            speed = TrackUtils::calcEndSpeed(1.0, speed, buckets[bucketIndex].length);
            if (speed > buckets[bucketIndex].maxSpeed) {
                break;
            }
            ++counter;
        }
        if (counter > maxCounter) {
            maxCounter = counter;
            maxId = prevPieceId;
        }
        //pieceInfos[prevPieceId].turboUsage = counter;
        counters.push_back(counter);
        pieceIds.push_back(prevPieceId);
    }

    //int maxCount = 0;
    //int maxId = -1;
    for (int i=0; i<counters.size(); ++i) {
        int counter = counters[i];
        bool isTurbo = counter * 120 >= maxCounter * 100;
        cout << "Turbo Pieces " << pieceIds[i]  << " count " << counter << " isTurbo " << isTurbo << endl;
        if (isTurbo) {
            pieceInfos[pieceIds[i]].isTurbo = isTurbo;
        }
    }
}

//void SpeedLimit::updateValues() {
//    //mergeLimits();
//}



/////////////////////////////////////////////////////////////

namespace TrackUtils {


 void setDistance(CarPosition2& pos, const Track2& track) {
    const Lane2& lane = track.lane2s[pos.laneIdStart];
    double curPieceTotalLength = lane.pieceLengths[pos.pieceId];
    double prevPieceTotalLength = pos.pieceId > 0 ? lane.pieceLengths[pos.pieceId-1] : 0;
    pos.totalDistance = pos.pieceDistance + prevPieceTotalLength;
    pos.laneId = pos.laneIdStart;

//    double startDistance;
//    double endDistance;
//    double deltaDistance;

    for (const auto& sector : track.sectors) {
       if (sector.laneId == pos.laneIdStart && pos.totalDistance >= sector.startDistance && pos.totalDistance <= sector.endDistance) {
         //cout << "Sector=" << sector.id << endl;
         pos.sectorId = sector.id;
       }
    }

    if (pos.laneIdStart != pos.laneIdEnd) {
       const auto& piece = track.pieceInfos[pos.pieceId];
       if (piece.turn != Turn::none) {
          //cout << "switch in turn " << (piece.turn == Turn::left ? "left" : "right") << endl;
          if (TrackUtils::getRadius(piece, track.laneInfos[pos.laneIdEnd]) < TrackUtils::getRadius(piece, track.laneInfos[pos.laneIdStart])) {
             pos.laneId = pos.laneIdEnd;
          }
       } else if (pos.pieceDistance > (curPieceTotalLength-prevPieceTotalLength)/2) {
         pos.laneId = pos.laneIdEnd;
       }
    }

    //if (pos.laneIdStart != pos.laneIdEnd && pos.pieceDistance > (curPieceTotalLength-prevPieceTotalLength)/2) {
    //   pos.laneId = pos.laneIdEnd;
    //}
 }


void updateCarPositions2(Track2& track, int deltaGameTicks, const jsoncons::json& msg) {
   if (deltaGameTicks < 0) {
      cout << "updateCarPositions: deltaGameTicks=" << deltaGameTicks << endl;
      return;
   }

   int size = msg.size();
   for (int i=0; i<size; ++i) {
      CarPosition2 newPosition;
      const auto& currObj = msg.at(i);
      string color = currObj["id"]["color"].as<string>();
      int carId = track.getCarIdFromColor(color);

      newPosition.angle = currObj["angle"].as<double>();
      newPosition.pieceId = currObj["piecePosition"]["pieceIndex"].as<int>();
      newPosition.pieceDistance = currObj["piecePosition"]["inPieceDistance"].as<double>();
      newPosition.laneIdStart = currObj["piecePosition"]["lane"]["startLaneIndex"].as<int>();
      newPosition.laneIdEnd = currObj["piecePosition"]["lane"]["endLaneIndex"].as<int>();
      newPosition.lap = currObj["piecePosition"]["lap"].as<int>();
      setDistance(newPosition, track);

      auto& car = track.carInfos[carId];
      car.updatePosition(track, newPosition, deltaGameTicks);
      //cout << "update car: id=" << carId << ", color=" << color << endl;
    }
}



Track2 createTrackNew(const json& msg) {
    CarInfos cars;
    const auto& jsonCars = msg["race"]["cars"];
    //{"dimensions":{"guideFlagPosition":10.0,"length":40.0,"width":20.0},"id":{"color":"blue","name":"Rosberg"}}
    int size = jsonCars.size();
    for (int i=0; i<size; ++i) {
        string color = (jsonCars.at(i)["id"])["color"].as<string>();
        cars.push_back(CarInfo{i, color});
    }

    LaneInfos lanes;
    const auto& jsonLanes = msg["race"]["track"]["lanes"];
    // {"distanceFromCenter": -20, "index": 0}
    size = jsonLanes.size();
    for (int i=0; i<size; ++i) {
        double distanceFromCenter = jsonLanes.at(i)["distanceFromCenter"].as<double>();
        int id = jsonLanes.at(i)["index"].as<int>();
        if (id != i) {
            cout << "ERROR: unexpected id: " << id << " at pos " << i << endl;
        }
        lanes.push_back(LaneInfo{i, distanceFromCenter, i-1, i<size-1 ? i+1 : -1});
    }
    for (const auto& info : lanes) {
        cout << "LaneInfo: id=" << info.id << ", dist=" << info.distanceFromCenter << ", left=" << info.laneToLeft << ", right=" << info.laneToRight << endl;
    }

    PieceInfos pieces;
    const auto& jsonPieces = msg["race"]["track"]["pieces"];
    // {"length": 100.0},{"length": 100.0,"switch": true},{"radius": 200,"angle": 22.5}
    size = jsonPieces.size();
    for (int i=0; i<size; ++i) {
        const auto& currObj = jsonPieces.at(i);
        bool isSwitch = currObj.has_member("switch") ? currObj["switch"].as<bool>() : false;
        double radius = 10000;
        double angle = 1;
        double length = -1;
        Turn turn = Turn::none;
        if (currObj.has_member("radius")) {
            radius = currObj["radius"].as<double>();
            angle = currObj["angle"].as<double>();

            turn = angle < 0 ? Turn::left : Turn::right;
            length = (2 * pi * radius * abs(angle)) / 360;
            //turn = angle < 0 ? Turn::left : Turn::right;
        } else {
            length = currObj["length"].as<double>();
            //pieces.push_back(PieceInfo{i, length, 10000, 45, isSwitch});
        }
        pieces.push_back(PieceInfo{i, turn, length, radius, angle, isSwitch});
    }
    for (const auto& info : pieces) {
        int t = info.turn == Turn::none ? 0 : (info.turn == Turn::left ? -1 : 1);
        cout << "PieceInfo: id=" << info.id << "turn=" << t << ", length=" << info.length << ", radius=" << info.radius << ", angle=" << info.angle << ", switch=" << info.isSwitch << endl;
    }

    // vector<double> SpeedLimit::simulateTime(const vector<double>& switchPositions)

    Lane2s lane2s;
    vector<Sector> sectors;
    vector<double> pieceLengths;
    vector<double> switchPositions;
    SpeedLimit speedLimit;

    //SpeedLimit speedLimitEstimate;
    for (const auto& lane : lanes) {
        double lengthFromStart = 0;
        switchPositions.push_back(0);
        for (const auto& piece : pieces) {
            double radius = TrackUtils::getRadius(piece, lane);
            double length = TrackUtils::getLength(piece, lane);
            //if (piece.turn != Turn::none) {
            //    double deltaRadius = lane.distanceFromCenter;
            //    double deltaLength = (2 * pi * lane.distanceFromCenter * abs(piece.angle)) / 360;
            //    radius += piece.turn == Turn::left ? deltaRadius : -deltaRadius;
            //    length += piece.turn == Turn::left ? deltaLength : -deltaLength;
            //}
            double maxSpeed = Physics::p().calcMaxSpeed(radius, 55);
            //double estimatedSpeed = calcEstimatedSpeed(radius, 55);

            //speedLimit.addPoint(SpeedPoint{lengthFromStart, maxSpeed, estimatedSpeed});
            //speedLimit.addBuckets(length, radius);

            // Use base length to get same number of buckets.
            int bucketCount = ceil(piece.length / maxBucketLength);
            speedLimit.addBuckets(piece.id, radius, bucketCount, maxSpeed, length, piece.turn != Turn::none);

            if (piece.isSwitch) {
                cout << "add  switch pos " << (lengthFromStart + length / 2) << endl;
               switchPositions.push_back(lengthFromStart + length / 2);
            }
            lengthFromStart += length;


            pieceLengths.push_back(lengthFromStart);
            //speedLimitMax.addPoint(SpeedPoint{lengthFromStart, maxSpeed});
            //speedLimitEstimate.addPoint(SpeedPoint{lengthFromStart, estimatedSpeed});
        }
        // Add extra point to close wrap of lap.

        //const auto& ptFirst = speedLimitMax.speedPoints[0];

        //speedLimitMax.addPoint(SpeedPoint{lengthFromStart, ptFirst.speedMax, ptFirst.speedEstimate});

        cout << "Pre adapt" << endl;
        //speedLimit.log();
        speedLimit.fix();
        //cout << "After adapt lane:" << lane.id << endl;
        //speedLimit.logLimit();

        lane2s.push_back(Lane2{lane.id, speedLimit, pieceLengths});

        for (int i=0; i<switchPositions.size(); ++i) {
           double currDistance = switchPositions[i];
           double endDistance = i<switchPositions.size()-1 ? switchPositions[i+1] : lengthFromStart;
           double deltaDistance = endDistance - currDistance;

           int sectorId = sectors.size();

           cout << "sector " << sectorId << " start=" << currDistance << " end=" << endDistance << " distance=" << deltaDistance << endl;

           int pieceId = pieces.size() - 1;
           int switchCount = i + 1;
           for (const auto& piece : pieces) {
             if (piece.isSwitch) {
                --switchCount;
             }
             if (switchCount <= 0) {
                pieceId = piece.id;
                break;
              }
          }

           sectors.push_back(Sector{sectorId, lane.id, pieceId, currDistance, endDistance, deltaDistance});
        }

        speedLimit.clear();
        pieceLengths.clear();
        switchPositions.clear();
    }

    int laneCount = lanes.size();
    int sectorsPerLane = sectors.size() / laneCount;

    for (int i=0; i<laneCount; ++i) {
       int baseSector = i * sectorsPerLane;
       for (int k=0; k<sectorsPerLane; ++k) {
          Sector& currSector = sectors[baseSector + k];
          int nextSectorId = (k + 1) % sectorsPerLane;
          // add straight
          currSector.addNextSector(baseSector + nextSectorId);
          if (k < sectorsPerLane - 1) {
             // add left
             if (lanes[currSector.laneId].laneToLeft >= 0) {
               currSector.addNextSector(sectorsPerLane * lanes[currSector.laneId].laneToLeft + nextSectorId);
             }
             // add right
             if (lanes[currSector.laneId].laneToRight >= 0) {
               currSector.addNextSector(sectorsPerLane * lanes[currSector.laneId].laneToRight + nextSectorId);
             }
          }
       }
    }

    for (int k=sectorsPerLane-2; k>=0; --k) {
      for (int i=0; i<laneCount; ++i) {
          Sector& currSector = sectors[i * sectorsPerLane + k];
          double minDistance = -1;
          int minIndex = -1;
          for (int n=0; n<currSector.nextSectors.size(); ++n) {
             int nextSectorIndex = currSector.nextSectors[n];
             double distance = sectors[nextSectorIndex].distanceToGoal;
             if (minIndex < 0 || distance < minDistance) {
                minDistance = distance;
                minIndex = n;
             }
          }
          currSector.closestIndex = minIndex;
          currSector.distanceToGoal = minDistance + currSector.deltaDistance;
       }
    }

    for (const auto& s : sectors) {
       cout << "sector id=" << s.id << ", lane=" << s.laneId << ", piece=" << s.pieceId << ", startPos=" << s.startDistance << ", endDist=" << s.deltaDistance;
       cout << ", minDist=" << s.distanceToGoal << ", closestId=" << s.closestIndex << ", links(";
       for (auto id : s.nextSectors) {
          cout << id << " ";
       }
       cout << ")" << endl;
    }

   //int numberOfLaps = msg["raceSession"]["laps"].as<int>();
   //cout << "TrackNumberoFLaps=" << numberOfLaps << endl;
   return Track2{lane2s, lanes, pieces, cars, sectors};
}


double calcEndSpeed(double throttle, double speed, double distance) {
    if (throttle == 0 && speed <= 0) {
        return 0;
    }
   int counter = 100;
   double currDistance = distance;
   double currSpeed = speed;
   double prevSpeed = currSpeed;
   double prevDistance = currDistance;
   while (--counter > 0) {
      currSpeed += Physics::p().calcDeltaSpeed(currSpeed, throttle);
      //if (currSpeed >= prevSpeed) {
      //    cout << "calcend currrSpeed:" << currSpeed << " prev " << prevSpeed << endl;
      //}
      currDistance -= currSpeed;
      if (currDistance <= 0) {
          double ratio = -currDistance / (prevDistance - currDistance);
          currSpeed = prevSpeed - ratio * (prevSpeed - currSpeed);
          if (throttle == 0 && speed <= currSpeed) {
              cout << "calcEndSpeed: dist=" << distance << " "  << speed << " -> " << currSpeed << " ratio: " << ratio << endl;
          }
          return currSpeed;
      }
      prevSpeed = currSpeed;
      prevDistance = currDistance;
   }

   if (throttle == 0 && speed <= currSpeed) {
        cout << "calcEndSpeed: dist=" << distance << " "  << speed << " -> " << currSpeed << endl;
   }
   return currSpeed;
}

double getLength(const PieceInfo& piece, const LaneInfo& lane) {
    double length = piece.length;
    if (piece.turn != Turn::none) {
        double deltaRadius = lane.distanceFromCenter;
        double deltaLength = (2 * pi * lane.distanceFromCenter * abs(piece.angle)) / 360;
        //radius += piece.turn == Turn::left ? deltaRadius : -deltaRadius;
        length += piece.turn == Turn::left ? deltaLength : -deltaLength;
    }
    return length;
}

double getRadius(const PieceInfo& piece, const LaneInfo& lane) {
    double radius = piece.radius;
    if (piece.turn != Turn::none) {
        double delta = lane.distanceFromCenter;
        radius += piece.turn == Turn::left ? delta : -delta;
    }
    return radius;
}

//double calcDeltaSpeed(double throttle, double speed) {
//    return  throttle / 5 - speed * 0.019816627;
//}

//double calcThrottle(double currSpeed, double targetSpeed) {
//    double deltaSpeed = targetSpeed - currSpeed;
//    double throttle =  5 * (deltaSpeed + currSpeed * 0.019816627);
//    return max(0.0, min(1.0, throttle));
//}

//double calcNextSpeed(double throttle, double speed, int steps) {
//    double nextSpeed = speed;
//    for (int i=0; i<steps; ++i) {
//        speed += calcDeltaSpeed(throttle, speed);
//    }
//    return speed;
//}

//double calcMaxSpeed(double throttle) {
//    return throttle * 10;
//}


//double calcMaxDeltaAngle(double radius, double speed) {
//    return -125.2243 + 395.7098 * speed * speed / radius;
//}

//double calcMaxSpeed(double radius, double maxAngle) {

//    double speed = sqrt(abs(radius) * (abs(maxAngle) + 125.2243) / 395.7098);
//    // todo: turbo?
//    return min(speed, Physics::p().maxSpeed()*2);
//}

//double calcEstimatedSpeed(double radius, double maxAngle) {
//    //double speed = sqrt(abs(radius) * (abs(maxAngle) + 125.2243) / 395.7098);
//    // todo: turbo?
//    return min(Physics::p().maxSpeed(), calcMaxSpeed(radius, maxAngle));
//}

} // namespace
