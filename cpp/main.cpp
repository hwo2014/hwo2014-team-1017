#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include "protocol.h"
#include "connection.h"
#include "connection_dummy.h"
#include "game_logic.h"
#include "util.h"

using namespace hwo_protocol;

void run(hwo_connection& connection, const std::string& name, const std::string& key)
{
  game_logic game;
  connection.send_requests({ make_join(name, key) });

  for (;;)
  {
    boost::system::error_code error;
    auto response = connection.receive_response(error);

    if (error == boost::asio::error::eof)
    {
      std::cout << "Connection closed" << std::endl;
      break;
    }
    else if (error)
    {
      throw boost::system::system_error(error);
    }

    bool hasReply = game.react(response);
    if (hasReply) {
        connection.send_requests(game.get_reply());
    }
   }
}

void run_dummy(const std::string& name, const std::string& key)
{
  game_logic game;
  dummy_connection dc;
  dc.send_requests({ make_join(name, key) });

  Timer timer;
  for (;;) {
        timer.reset(true);
    auto response = dc.receive_response();
    //std::cout << "response=" << response.to_string() << std::endl;
    const auto& msg_type = response["msgType"].as<std::string>();
    if (msg_type == "gameStart") {
        std::cout << "GameStart. set dummy tack" << std::endl;
        dc.setTrack(game.getTrack());
    }

    bool isEnd = false;
    if (msg_type == "gameEnd") {
        std::cout << "GameEnd" << std::endl;
        isEnd = true;
    }

    bool hasReply = game.react(response);
    if (isEnd) {
        cout << "got reply" << endl;
    }
    if (hasReply) {
        dc.send_requests(game.get_reply());
        if (isEnd) {
            cout << "sent request" << endl;
        }
    }
    if (isEnd) {
        std::cout << "GameEnd2" << std::endl;
        return;
    }
    //std::cout << "micros=" << timer.stop() << std::endl;
  }
}

int main(int argc, const char* argv[])
{
  try
  {
    std::cout << "argc=" << argc << std::endl;
    if (argc == 1) {
        std::cout << "Running dummy" << std::endl;
        run_dummy("Forrest", "mykey");
        return 0;
    }
    if (argc != 5)
    {
      std::cerr << "Usage: ./run host port botname botkey" << std::endl;
      return 1;
    }

    const std::string host(argv[1]);
    const std::string port(argv[2]);
    const std::string name(argv[3]);
    const std::string key(argv[4]);
    std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;

    hwo_connection connection(host, port);
    run(connection, name, key);
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 2;
  }

  return 0;
}
